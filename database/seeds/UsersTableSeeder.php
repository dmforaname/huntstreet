<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Admin',
                'email' => 'admin@example.com',
                'password' => bcrypt('password'),
                'is_admin'  => 1,
            ],
            [
                'name' => 'User',
                'email' => 'user@example.com',
                'password' => bcrypt('password'),
                'is_admin'  => 0,
            ]
        ];

        foreach ($data as $item) {
            
            $user = User::where('email',$item['email'])->first();

            if(!$user)
                User::firstOrCreate($item);
        }
    }
}
