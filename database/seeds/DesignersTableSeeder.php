<?php

use Illuminate\Database\Seeder;
use App\Models\Designer;

class DesignersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'Akira',],
            ['name' => 'Andrea Pompilio',],
            ['name' => 'Bloch',],
            ['name' => 'Catherine Malandrino'],
            ['name' => 'DKNY'],
            ['name' => 'Eytys'],
            ['name' => 'Fabiana Filippi'],
            ['name' => 'Fairfax'],
            ['name' => 'Farah Khan'],
            ['name' => 'Farm'],
            ['name' => 'I Pinco Pallino'],
            ['name' => 'Iceberg'],
            ['name' => 'Il Gufo'],
            ['name' => 'Inari'],
        ];

        foreach ($data as $item) {
            
            $designer = Designer::where('name',$item['name'])->first();

            if(!$designer)
                Designer::firstOrCreate($item);
        }
    }
}
