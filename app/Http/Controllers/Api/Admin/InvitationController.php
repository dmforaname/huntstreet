<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Invitation;
use Illuminate\Http\Request;
use App\Repositories\Api\Admin\InvitationRepository;
use App\Traits\ApiResponser;
use App\EloquentVueTables;
use App\Http\Requests\Admin\NotificationStore;
use App\Jobs\InvitationEmailJob;
use App\Notifications\InvitationMail;

class InvitationController extends Controller
{
    use ApiResponser;

    /**
     * @param InvitationRepository $div
     */
    public function __construct(InvitationRepository $invitation)
    {
        $this->invitation = $invitation;
    }    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->invitation->query();
        $data->with('user.designers');
        $data->latest();

        return (new EloquentVueTables())->get($data, ['*']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NotificationStore $request)
    {
        $data = $this->invitation->create($request->validated());

        // check if data are failed to saved
        if (!$data)
            return $this->error(trans('message.saving_error'),403);    
       
        // Send invitation email    
        $jobs = new InvitationEmailJob($data);
        dispatch($jobs);

        return $this->success($data,trans('message.create'),201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
