<?php

namespace App\Http\Controllers;

use Facade\FlareClient\View;
use Illuminate\Http\Request;
use App\Repositories\Api\Admin\InvitationRepository;
use App\Repositories\Web\RegisterRepository;
use App\Repositories\Web\DesignerRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Jobs\RegistrationEmail;
use App\Models\Designer;
use App\User;

class RegisterController extends Controller
{

    /**
     * @param InvitationRepository $div
     */
    public function __construct(
        InvitationRepository $invitation,
        RegisterRepository $register,
        DesignerRepository $designer
        )
    {
        $this->invitation = $invitation;
        $this->register = $register;
        $this->designer = $designer;
    }       

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->invitation_only){

            $invitation = $this->invitation->getData(request()->invitation_only);
            
            if ($invitation->first()){

                $designer = $this->designer->get();
                 
                $date = Carbon::parse($invitation->expired_at)->format('M d, Y H:i:s');

                $data['email'] = $invitation->email;
                $data['uuid'] = $invitation->uuid;
                $data['expired_at'] = $date;
                $data['designers'] = $designer;
                $data['registration_code'] = ($invitation->user_id == NULL) ? null : $invitation->user->uuid;

                return View('auth.register',$data);
            }
        }

        return redirect('/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->register->validator($request->all());

        if ($validator->fails()) {
            return redirect('register?invitation_only='.$request->uuid)
                        ->withErrors($validator)
                        ->withInput();
        }

        $data = $validator->validated();

        $data['password'] = Hash::make($data['password']);
        
        $create = $this->register->create($data);

        if ($create){

            // Add designer to users
            foreach($request->designers as $designer){
                $user = User::find($create->id);
                $user->designers()->attach($designer);
            }

            // Send email thank you
            $jobs = new RegistrationEmail($create);
            dispatch($jobs)->delay(now()->addMinutes(60));           

            // Update invitation foreign key
            $invitation = $this->invitation->uuid($request->uuid);
            $invitation->update(['user_id' => $create->id]);

            return redirect('register?invitation_only='.$request->uuid);    
        }
           

        return redirect('register?invitation_only='.$request->uuid)->with(['error' => 'Registration failed']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
