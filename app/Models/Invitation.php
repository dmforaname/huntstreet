<?php

namespace App\Models;
use App\Traits\Uuid as uuidTrait;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    use uuidTrait,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email','user_id'
    ];

    protected $appends = [
        'expired_at'
    ];

    /**
     * Set expired date.
     *
     * @return void
     */
    public function getExpiredAtAttribute()
    {
        $date = Carbon::parse($this->created_at);

        return $date->addMonth()->toDateTimeString();
    }

    /**
     * Get the users record.
     */
    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }

    /**
     * Route notifications for the mail channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return array|string
     */
    public function routeNotificationForMail($notification)
    {
        // Return email address 
        return [$this->email];
    }
}
