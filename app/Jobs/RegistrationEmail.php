<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Notification;
use App\Notifications\RegisterNotification;

class RegistrationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $parameters;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $details = [

            'greeting' => 'Hi '.$this->parameters->name,
            'body' => 'This is your registration code',
            'thanks' => 'Thank you!',
            'actionText' => 'registration code',
            'regisCode' => $this->parameters->uuid,
            'data_id' => $this->parameters->id,
        ];
        
        Notification::send($this->parameters, new RegisterNotification($details));
    }
}
