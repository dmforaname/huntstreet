<?php

namespace App\Repositories\Api\Admin;

use App\Models\Invitation;
use App\Repositories\BaseRepository;

class InvitationRepository extends BaseRepository
{
    protected $model;

    /**
     * Repository constructor.
     *
     * @param  Invitation  $mod
     */
    public function __construct(Invitation $mod)
    {
        $this->model = $mod;
    }

    public function getData($uuid)
    {
        return $this->model->with('user')->Uuid($uuid);
    }
}
