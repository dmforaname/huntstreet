<?php

namespace App\Repositories\Web;

use App\Models\Designer;
use App\Repositories\BaseRepository;

class DesignerRepository extends BaseRepository
{
    protected $model;

    /**
     * Repository constructor.
     *
     * @param  Designer  $mod
     */
    public function __construct(Designer $mod)
    {
        $this->model = $mod;
    }
}
