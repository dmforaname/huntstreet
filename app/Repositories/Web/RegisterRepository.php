<?php

namespace App\Repositories\Web;

use App\User;
use App\Repositories\BaseRepository;
use Validator;

class RegisterRepository extends BaseRepository
{
    protected $model;

    /**
     * Repository constructor.
     *
     * @param  User  $mod
     */
    public function __construct(User $mod)
    {
        $this->model = $mod;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'birth_date' => ['required','date'],
            'gender' => ['required'],
        ]);
    }
}
