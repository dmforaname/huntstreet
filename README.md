## Requirements

PHP 7.2.*

## Install

git clone project

```shell
git clone https://dmforaname@bitbucket.org/dmforaname/huntstreet.git
```

Create and edit `.env`

```shell
cp .env.example .env
vim .env
```

Edit the following env variables

```env
APP_NAME=
APP_KEY=
APP_URL=
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=
QUEUE_CONNECTION=database
```


## Install dependencies:

```shell
composer install
npm install && npm run dev 
php artisan config:cache
php artisan key:generate
php artisan migrate
php artisan db:seed
php artisan passport:install
```

## Admin login

```shell
user : admin@example.com
pass : password
```