<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes([
    'register' => false, 
    'verify' => false, 
  ]);


Route::resource('/register', 'RegisterController')
        ->only(['index','show','store','update','destroy']);
        
Route::get('/loggedin', 'HomeController@admin');
Route::get('/loggedout', 'HomeController@admin');


Route::middleware(['auth', 'auth-check'])->group(function () {

    Route::get('/', 'HomeController@index')->name('home');
    
    Route::get('/admin', 'HomeController@admin')->where('app', '.*');
    Route::get('/admin/{any}', 'HomeController@admin')->where('app', '.*');
});