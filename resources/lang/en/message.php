<?php

return [

   'create' => 'Successfully Saved',
   'retrieve' => 'Retrieved successfully',
   'update' => 'Data Updated',
   'saving_error' => 'Saving Error',
   'delete_failed' => 'Failed to delete',
   'delete' => 'Successfully Deleted',
   'error' => 'error occurred',
   'birth_date' => 'Birth Date',
   'gender' => 'Gender',
   'designer' => 'Designer'
];