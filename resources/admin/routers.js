import Dashboard from "./pages/Dashboard"
import Invitation from "./components/admin/Invitation"

const routes =  [
    {
        path: "/admin",
        alias: '/admin/dashboard',
        name: 'Dashboard',
        component: Dashboard,
    },
    {
        path: "/admin/invitation",
        name: 'Invitation',
        component: Invitation,
        meta: {
            
            title : 'User Invitation'
        }
    },
];

export default routes;
