
import Vue from "vue";
import Vuex from 'vuex'

Vue.use(Vuex)


export default new Vuex.Store({ 
    
    state : { 

        token: localStorage.getItem('token') || '',

    },
    mutations : { 

        login(state, token) {
            const now = new Date()

            localStorage.setItem("token", token)
            localStorage.setItem("token_ttl", now.getTime() + (3*60*60*1000))
        },
        logout(state) {
            localStorage.removeItem("token")
            localStorage.removeItem("token_ttl")
        },

    },
    getters: {
        isAuth: state => {
            return localStorage.getItem('token') !== null
        }
    },
    actions: { 

    }
})